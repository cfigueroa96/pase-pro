<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Create Word File in Laravel</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  

  <script src="https://cdn.jsdelivr.net/npm/vue"></script> 
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

  </head>
  <body>
    <div class="container" id="app">
      <h2 class="text-center">Generar pases a productivo </h2>
      <form method="post" action="store">
        {{ csrf_field() }}

        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="Name">Ingresa tu nombre:</label>
            <input type="text" class="form-control" name="name" >
          </div>
        </div>

        <div class="row" >
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="Name">Solicitante:</label>
            <input type="text" class="form-control" name="applicant">
          </div>
        </div>

        <div class="row" v-for="position in order">
          <div class="form-group col-md-3">
            <label for="Name">Orden</label>
            <input type="text" class="form-control" name="order[]" :value="position.order" readonly="true">
          </div>

          <div class="form-group col-md-5">
            <label for="Name">Descripción</label>
            <input type="text" class="form-control" name="description[]" :value="position.description" readonly="true">
          </div>

          <div class="form-group col-md-2">
            <label for="Name">Caso</label>
            <input type="text" class="form-control" name="case" v-model="caseNumber">
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-12">
            <button type="submit" class="btn btn-success btn-block">Generar documento</button>
          </div>
        </div>


      </form>


      <input type="text" class="form-control" placeholder="Buscar orden.." v-model="search" name="">

      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Orden</th>
            <th scope="col">Descripción</th>
            <th scope="col">Fecha</th>
          </tr>
        </thead>
        <tbody>
          <tr v-if="load == true">
            <td colspan="3">
              <h3>Cargando ordenes por favor espere..</h3>
            </td>
          </tr>
          <tr 
            v-for="(o, index) in searchOrder"  
            :key="index"
            v-if="load == false"
          >
            <td>
              <input type="checkbox" v-model="order" :value="o">
            </td>
            <td>@{{ o.order }}</td>
            <td>@{{ o.description }}</td>
            <td>@{{ o.date_act }}</td>
          </tr>
        </tbody>
      </table>

   </div>



   <script>
        var app = new Vue({
          el: '#app',
          created: function() {
            this.orders();
          },
          data: {
            order: [],
            count:1,
            or:[],
            load:false,
            caseNumber:null,
            search:'',
          },
          computed: {
            searchOrder() {

              let self = this;

              return this.or.filter((item) => {
                return item.order.toLowerCase().indexOf(self.search.toLowerCase()) >= 0
                    || item.description.toLowerCase().indexOf(self.search.toLowerCase()) >= 0;
              })
            }
          },
          methods: {
            orders() {

              this.load = true;

              axios.get('orders/all')
                .then(response => {
                  this.or = response.data;
                })
                .finally(() => {
                  this.load = false;
                })
            }
          }
        })
   </script>
  </body>
</html>