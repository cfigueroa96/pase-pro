<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('create','DocumentController@create');
Route::post('store','DocumentController@store');

Route::resource('excel', 'ExcelController');
Route::resource('orders', 'OrdersController');