<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('welcome');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name           =  $request['name'];
        $order          =  $request['order'];
        $description    =  $request['description'];
        $case           =  $request['case'];
        $date           =  date("d.m.Y");
        $nameDocument   = $name."-".$order[0]."-".$date.".docx";

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        $section->addImage("./logo/Logo-Plumrose.png", array('width' => 60, 'height' => 60)); 

        $section->addText(
            "________________________________________________________________________________",
        );

        $section->addText(
            "PLUMROSE Latinoamericana,C.A.                                                                               
             SOLICITUD DE CONTROL DE CAMBIO AL SISTEMA                                                         
             
             Fecha de Solicitud: ".$date.". Nro.: ".$case."____                                                                                 

             Nombre: ".$name.". Departamento: Gerencia de Sistemas para Manufactura y Procura.",
            array('name'=>'Arial','size' => 10,'bold' => true,)
        );


        $section->addText(
            "Cambio Afecta a: _X_SAP.",
            array('name'=>'Arial','size' => 10,'bold' => true, )
        );

        
        $section->addText(
            "Fecha de planificación del cambio: ".$date.".  Hora planificada del Cambio: _03:19:__. Duración:_____(Hrs).",
            array('name'=>'Arial','size' => 10,'bold' => true, )
        );

        $section->addText(
            "Tipo de Cambio:    X__Planificado. ___Emergente. ___Preventivo___Correctivo.",
            array('name'=>'Arial','size' => 10,'bold' => true)
        );

        $section->addText(
            "Prioridad: x__Alta. __Media. ___Baja.",
            array('name'=>'Arial','size' => 10,'bold' => true)
        );


        $section->addText(
            "Ejecutar previamente los Cambios Nro.: _______, _______, _______,
_______, _______, _______, _______, _______, _______, _______.
",
            array('name'=>'Arial','size' => 10,'bold' => true)
        );

        $section->addText(
            "Cambio exitoso: _X__Si. ___No. Fecha próximo Cambio: ___.___.___.",
            array('name'=>'Arial','size' => 10,'bold' => true)
        );


        $section->addText(
            "Cambios Suspendidos Nros.: _______, _______, _______, _______, _______,
_______, _______, _______, _______, _______, _______, _______, _______.
",
            array('name'=>'Arial','size' => 10,'bold' => true)
        );


        $section->addText(
            "Motivo del Cambio: _ Nuevo Requerimiento.  X __ Modificación. ___Mantenimiento. 
 ___ Desincorporación. ___Pruebas. ___Mudanza ___Demostración. ___Simulación. ___Entonación. _Otro: ________________________________________________________.
",
            array('name'=>'Arial','size' => 10,'bold' => true)
        );


        $section->addText(
            "Descripción del Cambio (Beneficio/Justificación): ",
            array('name'=>'Arial','size' => 10,'bold' => true)
        );

        $tableStyle = array(
            //'borderColor' => '006699',
            'borderSize'  => 1,
            'cellMargin'  => 10,
        );

        $phpWord->addTableStyle('myTable', $tableStyle);
        $styleCell = array(
            'bgColor' => 'FEFF9F',
            'cellMargin'  => 10
        );

        $styleText = array(
            'bold'    => true,
            'size'    => 8
        );

        $styleTextBody  = array(
            'size'    => 9,
            'align' => 'center'
        );

        $textCenter = array(
            'align' => 'center',
            'spaceAfter' => 50,
            'spaceBefore' => 50
        );

        $table = $section->addTable('myTable');
            $table->addRow();
                $table->addCell(2000, $styleCell)->addText('Nª ORDEN', $styleText, $textCenter  );
                $table->addCell(10000, $styleCell)->addText('DESCRIPCIÒN', $styleText, $textCenter  );
                $table->addCell(2000, $styleCell)->addText('CASO', $styleText, $textCenter  );
                $table->addCell(2000, $styleCell)->addText('SEC', $styleText, $textCenter  );
            for ($i=0; $i < count($order) ; $i++) { 
                $table->addRow();
                    $table->addCell(2000)->addText($order[$i], $styleTextBody, $textCenter  );
                    $table->addCell(10000)->addText($description[$i], $styleTextBody, $textCenter  );
                    $table->addCell(2000)->addText($case, $styleTextBody, $textCenter  );
                    $table->addCell(2000)->addText($i + 1, $styleTextBody, $textCenter  ); 
            }



        $section->addText();
        $section->addText();

        $section->addText(
            "Asignado a: ______________________________. Teléfono:__________________.",
            array('name'=>'Arial','size' => 10,'bold' => true, 'spacingLineRule' => 'atLeast')
        );


        $section->addText(
            "Analista de Respaldo: ".$request['name']."                                                                                        

            Observaciones:                                                                                                                

            Solicitante:  ".$request['applicant'],
            array('name'=>'Arial','size' => 10,'bold' => true)
        );






       // $section->addImage("./images/Krunal.jpg");  
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save($nameDocument);
        return response()->download(public_path($nameDocument));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
